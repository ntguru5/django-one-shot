from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": todo_detail,
    }
    return render(request, "todos/detail.html", context)


# created new list without using form class
def todo_list_create(request):
    if request.method == "POST":
        name = request.POST.get("name")
        if name:
            TodoList.objects.create(name=name)
            return redirect("todo_list_list")
    return render(request, "todos/create.html")


def todo_list_update(request, id):
    todo_update = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_update)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoListForm(instance=todo_update)
    context = {
        "form": form,
        "todo_update": todo_update,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    todo_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_delete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")
